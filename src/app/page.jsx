import "./globals.css";
import Config from "../../config/default";
import HomeScreen from "../Components/Home/homeScreen";

const getHomeData = async () => {
  try {
    const response = await fetch(Config.API.HOME.GET, {
      cache: "no-store",
    });
    return await response.json();
  } catch (error) {
    console.log("Error on Home SSR", error);
  }
};
const Home = async () => {
  const homeData = await getHomeData();

  const { data } = homeData;
  return <HomeScreen data={data} />;
};
export default Home;
