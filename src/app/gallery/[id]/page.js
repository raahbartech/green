import React from "react";
import Config from "../../../../config/default";
import GalleryContent from "@/Components/Gallery/galleryContent/galleryContent";

const getGalleryItems = async (parentId) => {
  const response = await fetch(
    Config.API.GALLERY.GET + `?parentId=${parentId}`
  );

  if (response.status === 200) {
    return await response.json();
  }
};

const Page = async ({ params }) => {
  const { id } = params;
  if (!id) {
    return <>Not found</>;
  }

  const data = await getGalleryItems(id);

  return <GalleryContent data={data} id={id} />;
};

export default Page;
