"use client";
import "./index.css";
import pic1 from "../../../public/img/maleki.jpg";
import Image from "next/image";
import { useTranslation } from "react-i18next";

function About_us() {
  const { t } = useTranslation("about-us");
  return (
    <div id="about">
      <section className="about-us">
        <div className="top-pic">
          <div className="container">
            <div className="row">
              <div className="col6">
                <div className="title-box">
                  <h2 className="title-h font-800">{t("about-us")}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="First-p">
          <div className="container ">
            <div className="row">
              <div className="col-12">
                <div className="First-p-box">
                  <div className="first-p-box-h-box">
                    <div className="col-5">
                      <hr className="hr-first" />
                    </div>
                    <h4 className="col-2 first-p-box-h">
                      {t("first-and-last")}
                    </h4>
                    <div className="col-5">
                      <hr className="hr-first" />
                    </div>
                  </div>
                  <div className="first-p-box-p-box">
                    <p className="first-p-box-p">{t("description-about-us")}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="aboutus-box">
          {/* must set background image */}
          <div className="container ">
            <div className="row">
              <div className="col-md-6 col-12">
                <div className="aboutus-data">
                  <div className="aboutus-data-h-box">
                    <h3 className="aboutus-data-h">{t("about-green")}</h3>
                    <hr className="flex-fill hr-first" />
                  </div>
                  <div className="aboutus-data-p-box">
                    <p className="aboutus-data-p">
                      {t("description-about-green")}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="aboutus-2-box">
          {/* must set background image */}
          <div className="container ">
            <div className="row justify-content-end ">
              <div className="col-md-6 col-12">
                <div className="aboutus-2-data">
                  <div className="aboutus-2-data-h-box">
                    <h3 className="aboutus-2-data-h">{t("glass-wool")}</h3>
                    <hr className="flex-fill hr-first" />
                  </div>
                  <div className="aboutus-2-data-p-box">
                    <p className="aboutus-2-data-p">
                      {t("description-about-glass-wool")}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="Heads">
          <div className="container ">
            <div className="row">
              <div className="col-5">
                <hr className="hr-first" />
              </div>
              <h4 className="Heads-h col-2">{t("senior-managers")}</h4>
              <div className="col-5">
                <hr className="hr-first" />
              </div>
              <div className="col-md-4 col-12">
                <div className="Card">
                  <div className="Card-item">
                    <Image
                      src={pic1}
                      alt="Sunset in the mountains"
                      className="Card-img"
                    />
                    <div className="Card-data">
                      <p className="Card-title">{t("managers-name")}</p>
                      <p className="Card-p">{t("manager-position")}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-12">
                <div className="Card">
                  <div className="Card-item">
                    <Image
                      src={pic1}
                      alt="Sunset in the mountains"
                      className="Card-img"
                    />
                    <div className="Card-data">
                      <p className="Card-title">{t("managers-name")}</p>
                      <p className="Card-p">{t("manager-position")}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-12">
                <div className="Card">
                  <div className="Card-item">
                    <Image
                      src={pic1}
                      alt="Sunset in the mountains"
                      className="Card-img"
                    />
                    <div className="Card-data">
                      <p className="Card-title">{t("managers-name")}</p>
                      <p className="Card-p">{t("manager-position")}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
export default About_us;
