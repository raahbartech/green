'use client'
import React from 'react';
import StartVideo from '../elements/StartVideo';
import Cards from '../elements/Cards';
import MidVideo from '../elements/MidVideo';
import FinalVideo from '../elements/FinalVideo';
import News from '../News';
import Representatives from '../Representatives';
import Count from './Count';
import LastProject from '../LastProject';


const HomeScreen = ({ data }) => {
    return (
        <>
            <StartVideo />
            <Cards data={data?.products} />
            <MidVideo />
            <News data={data?.news} />
            <FinalVideo />
            <Representatives />
            <Count />
            <LastProject data={data?.projects} />
        </>
    );
}

export default HomeScreen;