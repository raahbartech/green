import Link from "next/link";
import "../Representatives/index.css";
import { useTranslation } from "react-i18next";

function Representatives() {
  const { t } = useTranslation("namayandegi-forosh-content");
  return (
    <>
      <section className="Representatives">
        <div className="container">
          <div className="row">
            <div className="Representatives-div">
              <div className="Representatives-div-p">
                <span>{t("access-green-representatives")}</span>
              </div>
              <div className="Representatives-div-btn">
                <Link href="/namayandegi-forosh">
                  <button className="btn btn-1 strat-title-btn ">
                    {t("representatives-list")}
                  </button>
                </Link>
                <Link href={"/contact-us"}>
                  <button className="btn btn-2 strat-title-btn">
                    {t("representation-request")}
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
export default Representatives;
